package com.kaewmanee.week5;

public class SelecionSortApp {
    public static void main(String[] args) throws Exception {
        int arr[] = new int[1000];
        randomArray(arr);
        selecionSort(arr);
        print(arr);

    }

    private static void print(int[] arr) {
        for(int a:arr) {
            System.out.print(a+" ");

        }
        System.out.println();
    }

    private static void randomArray(int[] arr) {
        for(int i = 0; i < arr.length;i++) {
            arr[i] = (int)(Math.random()*10000);
        }
    }

    public static int findMinIndex(int[] arr, int pos) {
        int minIndex = pos;
        for (int i = pos + 1; i < arr.length; i++) {
            if (arr[minIndex] > arr[i]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static void swap(int[] arr, int first, int secound) {
        int temp = arr[first];
        arr[first] = arr[secound];
        arr[secound] = temp;

    }

    public static void selecionSort(int[] arr) {
        int minIndex = 0;
        for (int pos = 0; pos < arr.length - 1; pos++) {
            minIndex = findMinIndex(arr, pos);
            swap(arr, minIndex, pos);
        }

    }

}
